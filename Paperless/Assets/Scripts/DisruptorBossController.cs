﻿using UnityEngine;
using System.Collections;

public class DisruptorBossController : MonoBehaviour {
	
	public float moveSpeed;
	public bool moveRight;
	public Transform wallCheck;
	public float wallCheckRadius;
	public LayerMask whatIsWall;
	public Transform edgeCheck;
	public Transform firePoint;
	public GameObject mailAttackObject;
	public GameObject player;
	
	private bool hittingWall;
	private bool notAtEdge;
	private Animator anim;
	private float maxDistance = 40;
	private float minDistance = 1;
	private float attackTimer = 0;
	private bool attacked = false;
	//private Vector3 relativePointOfPlayer;

	
	void Start () {
		anim = GetComponent<Animator> ();
	}
	
	void Update () {
		hittingWall = Physics2D.OverlapCircle (wallCheck.position, wallCheckRadius, whatIsWall);
		notAtEdge = Physics2D.OverlapCircle (edgeCheck.position, wallCheckRadius, whatIsWall);
		//relativePointOfPlayer = player.transform.InverseTransformPoint(transform.position);

		if (attacked) {
			attackTimer = 0;
			attacked = false;
		}
		else
			attackTimer += Time.deltaTime;

		if ((Vector3.Distance(transform.position, player.transform.position) >= minDistance && Vector3.Distance(transform.position, player.transform.position) <= maxDistance) && attackTimer >= 1.4) { //timer == 50
			for (int i = 0; i < 3; i++)
					Instantiate(mailAttackObject, firePoint.position, firePoint.rotation);
			attacked = true;
		}
		
		if (hittingWall || !notAtEdge)
			moveRight = !moveRight;

		anim.SetFloat("Speed", Mathf.Abs (GetComponent<Rigidbody2D>().velocity.x));

		if (moveRight) {
			transform.localScale = new Vector3(-1f, 1f, 1f);
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (moveSpeed, GetComponent<Rigidbody2D> ().velocity.y);
		} 
		else {
			transform.localScale = new Vector3(1f,1f, 1f);
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (-moveSpeed, GetComponent<Rigidbody2D> ().velocity.y);
		}
	}
}
