﻿using UnityEngine;
using System.Collections;

public class EnemyHealthManager : MonoBehaviour {


	public int enemyHealth;
	public GameObject deathEffect;


	void Update () {
	
		if (enemyHealth <= 0) {


			Instantiate (deathEffect, transform.position, transform.rotation);
			Destroy(gameObject);

		}

	}
	public void giveDamage(int damageToGive)
	{

		enemyHealth -= damageToGive;

	}
}
