﻿using UnityEngine;
using System.Collections;

public class DisruptorMailAttackController : MonoBehaviour {

	public float speed;
	public DisruptorBossController disruptor;
	public GameObject impactEffect;
	public GameObject enemyPaper;
	public int floatingHeight;
	
	void Start () {
		disruptor = FindObjectOfType<DisruptorBossController>();

		if (Random.Range(-100,100) > 0)
			speed = Random.Range(1,7);
		else
			speed = Random.Range(-7,-1);
		//if(disruptor.transform.localScale.x > 0)
			//speed = -speed;

		GetComponent<Rigidbody2D>().velocity = new Vector2(speed, Random.Range(floatingHeight-4, floatingHeight+4));
	}
	
	void Update () {
		GetComponent<Rigidbody2D>().velocity = new Vector2(speed, GetComponent<Rigidbody2D>().velocity.y);
	}
	
	void OnTriggerEnter2D(Collider2D other) {
		if(other.tag == "Player") {
			Instantiate(impactEffect, other.transform.position, other.transform.rotation);
		}
		if(other.tag != "Enemy") {
			Instantiate(impactEffect, transform.position, transform.rotation);
			if (Random.Range(1, 25) == 1) {
				Instantiate(impactEffect, transform.position, transform.rotation);
				Instantiate(enemyPaper, transform.position, transform.rotation);
			}
			Destroy (gameObject);
		}
	}
}
