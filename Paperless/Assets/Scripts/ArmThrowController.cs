﻿using UnityEngine;
using System.Collections;

public class ArmThrowController : MonoBehaviour {

	public float speed;
	public PlayerController player;
	public GameObject enemyDeathEffect;
	public GameObject impactEffect;
	public int damageToGive;

	private float timer = 0;

	void Start () {
		player = FindObjectOfType<PlayerController>();

		if(player.transform.localScale.x > 0)
			speed = -speed;
	}

	void Update () {
		timer += Time.deltaTime;
		if (timer >= 1.5) {
			Instantiate(impactEffect, transform.position, transform.rotation);
			Destroy (gameObject);
		}
		GetComponent<Rigidbody2D>().velocity = new Vector2(speed, GetComponent<Rigidbody2D>().velocity.y);
	}

	void OnTriggerEnter2D(Collider2D other) {
		if(other.tag == "Enemy") {
			//Instantiate(enemyDeathEffect, other.transform.position, other.transform.rotation);
			//	Destroy (other.gameObject); 
			other.GetComponent<EnemyHealthManager> ().giveDamage(damageToGive);
		
		}
		Instantiate(impactEffect, transform.position, transform.rotation);
		Destroy (gameObject);
	}
}
