﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float moveSpeed;
	public float jumpHeight;
	public Transform groundCheck;
	public float groundCheckRadius;
	public LayerMask whatIsGround;
	public Transform firePoint;
	public GameObject armThrow;

	private float moveVelocity;
	private bool grounded;
	private bool doubleJumped;
	private bool facingLeft = true;
	private Animator anim;
	private float attackTimer = 2;
	
	void Start () {
		anim = GetComponent<Animator> ();
	}

	void FixedUpdate() {
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundCheckRadius, whatIsGround);
		float h = Input.GetAxis ("Horizontal");

		if (h > 0 && facingLeft) 
			Flip ();
		if (h < 0 && !facingLeft)
			Flip ();
	}

	void Update () {
		attackTimer += Time.deltaTime;
		if (grounded)
			doubleJumped = false;

		anim.SetBool ("Grounded", grounded);

		if ((Input.GetKeyDown (KeyCode.Space) || Input.GetKeyDown (KeyCode.UpArrow)) && grounded) { //Input.GetKeyDown (KeyCode.Space) || Input.GetKeyDown (KeyCode.UpArrow) && grounded
			Jump ();
			anim.SetTrigger("Jumped");
		}

		if ((Input.GetKeyDown (KeyCode.Space) || Input.GetKeyDown (KeyCode.UpArrow)) && !doubleJumped && !grounded) { //(Input.GetKeyDown (KeyCode.Space) || Input.GetKeyDown (KeyCode.UpArrow) && !doubleJumped && !grounded)
			Jump ();
			anim.SetTrigger("Jumped");
			doubleJumped = true;
		}

		moveVelocity = 0f;

		if (Input.GetKey (KeyCode.RightArrow)) {
			moveVelocity = moveSpeed;
			//GetComponent<Rigidbody2D> ().velocity = new Vector2 (moveSpeed, GetComponent<Rigidbody2D> ().velocity.y);
		}

		if (Input.GetKey (KeyCode.LeftArrow)) {
			moveVelocity = -moveSpeed;
			//GetComponent<Rigidbody2D> ().velocity = new Vector2 (-moveSpeed, GetComponent<Rigidbody2D> ().velocity.y);
		}

		GetComponent<Rigidbody2D>().velocity = new Vector2 (moveVelocity, GetComponent<Rigidbody2D> ().velocity.y);

		anim.SetFloat("Speed", Mathf.Abs (GetComponent<Rigidbody2D>().velocity.x));

		if(Input.GetKeyDown(KeyCode.Q) && attackTimer >= 1) { //Timer == 40
			Instantiate(armThrow, firePoint.position, firePoint.rotation);
			attackTimer = 0;
		}
	}

	void Flip () {
		facingLeft = !facingLeft;

		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	public void Jump() {
		GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, jumpHeight);
	}
}
