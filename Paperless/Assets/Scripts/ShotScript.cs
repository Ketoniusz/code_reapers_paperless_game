﻿using UnityEngine;
using System.Collections;

public class ShotScript : MonoBehaviour {


	public bool isEnemyShot = false;
	public int damageToGive;
	public GameObject impactEffect;
	double timeIsUp = 2.0 ;
	double timeNow = 1.0 ;
	void Start () {
	//	yield return new WaitForSeconds(2);
	//	Instantiate(impactEffect, transform.position, transform.rotation);
	//	Destroy (gameObject);
	}

	void timer(){
		if(timeNow >= timeIsUp){
			Instantiate(impactEffect, transform.position, transform.rotation);
				Destroy (gameObject); }
		if(timeNow < timeIsUp){
			timeNow += Time.deltaTime ; }
		Debug.Log(timeNow) ;
	}
	void OnTriggerEnter2D(Collider2D other) {
	

			if(other.tag == "Player") {
			Instantiate(impactEffect, other.transform.position, other.transform.rotation);
			HealthManager.HurtPlayer (damageToGive);
		}
			if(other.tag != "Enemy") {
			Instantiate(impactEffect, transform.position, transform.rotation);
				Destroy (gameObject);
			}


	

}
}