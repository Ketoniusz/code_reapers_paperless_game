﻿using UnityEngine;
using System.Collections;

public class EnemyPatrolJump : MonoBehaviour {
	
	public bool moveRight;
	public Transform wallCheck;
	public float wallCheckRadius;
	public LayerMask whatIsWall;
	//public Transform edgeCheck;
	
	private bool hittingWall;
	//private bool notAtEdge;
	private float jumpTimer = 0;
	private float jumpHeight = 20;

	void Start () {
	}

	void Update () {
		jumpTimer += Time.deltaTime;
		hittingWall = Physics2D.OverlapCircle (wallCheck.position, wallCheckRadius, whatIsWall);
		//notAtEdge = Physics2D.OverlapCircle (edgeCheck.position, wallCheckRadius, whatIsWall);

		if (hittingWall) //|| !notAtEdge
			moveRight = !moveRight;
		if (jumpTimer >=1) { //timer == 60
			jumpTimer = 0;
			if (Random.Range(0,10) > 5)
				Jump(Random.Range(2,6));
			else
				Jump(Random.Range(-6,-2));
		}
	}

	public void Jump(int moveSpeed) {
		GetComponent<Rigidbody2D>().velocity = new Vector2(moveSpeed , jumpHeight);
	}
}
